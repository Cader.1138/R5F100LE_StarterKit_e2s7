/*
 * ux_uart.h
 *
 *  Created on: Apr 12, 2017
 *      Author: developer
 */

#ifndef UX_UART_H_
#define UX_UART_H_

/* UART status list definition */
#define MD_UARTBASE         (0x100U)
#define MD_TX_BUSY          (MD_UARTBASE + 0x00U)  /* UART is busy */
#define MD_TX_EMPTY         (MD_UARTBASE + 0x01U)  /* Transmit buffer has space for at least one character */
#define MD_RX_DATA          (MD_UARTBASE + 0x02U)  /* Receive buffer has for at least one character */
#define MD_RX_WAITING       (MD_UARTBASE + 0x03U)  /* Waiting for RX character to arrive */

#define DBG_PUTS(str) do {static const __far char p[]={str}; u0_Puts(p);}while(0)

MD_STATUS u0_Putc(const unsigned char c);
MD_STATUS u0_Puts(const __far unsigned char * c);
MD_STATUS u0_PutcNonBlocking(const unsigned char c);
MD_STATUS u0_GetcNonBlocking(unsigned char * c);

MD_STATUS u2_Putc(const unsigned char c);
MD_STATUS u2_Puts(const __far unsigned char * c);
MD_STATUS u2_PutcNonBlocking(const unsigned char c);
MD_STATUS u2_GetcNonBlocking(unsigned char * c);

#endif /* UX_UART_H_ */
