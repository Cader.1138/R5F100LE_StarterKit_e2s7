/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_main.c
* Version      : CodeGenerator for RL78/G13 V2.05.01.04 [18 May 2018]
* Device(s)    : R5F100LE
* Tool-Chain   : GCCRL78
* Description  : This file implements main function.
* Creation Date: 8/11/2018
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_intc.h"
#include "r_cg_serial.h"
#include "r_cg_adc.h"
#include "r_cg_timer.h"
#include "r_cg_wdt.h"
#include "r_cg_rtc.h"
#include "r_cg_it.h"
#include "r_cg_pclbuz.h"
/* Start user code for include. Do not edit comment generated here */
/* Header file for the UART interface */
#include "ux_uart.h"
/* Header file for controlling the LCD interface. */
#include "lcd.h"
/* Header file with integer to string conversion functions */
#include "utility.h"
/* Define the RSK short name */
#define NICKNAME "RL78G13 "

void WaitForTicks(unsigned long WaitTicks);
void End_of_Test(void);

/* Static test function prototype */
void Statics_Test(void);
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
void R_MAIN_UserInit(void);

/***********************************************************************************************************************
* Function Name: main
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void main(void)
{
    R_MAIN_UserInit();
    /* Start user code. Do not edit comment generated here */
    WaitForTicks(1u);

    DBG_PUTS("\r\nYR0K50100LS000BE R5F100LE Starter Kit\r\nDemo v1.0 e2s_v7 "__DATE__" "__TIME__"\r\n") ;

    /* Initialize the LCD module. */
    Init_LCD();

    /* Display information on the debug LCD. */
    Display_LCD(LCD_LINE1, "Renesas");
    Display_LCD(LCD_LINE2, NICKNAME);

    /* Flash the user LEDs for some time or until a push button is pressed. */
    DBG_PUTS("Begin LED chasing test\r\n");
    Flash_LEDs();

    /* Flash the user LEDs at a rate set by the user potentiometer (ADC) using interrupts. */
    R_ADC_Set_OperationOn();
    R_ADC_Start();

    /* Demonstration of initialized variables. Use this function with the debugger.*/
    Statics_Test();

    /* Halt program in an infinite while loop */
    End_of_Test();
    /* End user code. Do not edit comment generated here */
}


/***********************************************************************************************************************
* Function Name: R_MAIN_UserInit
* Description  : This function adds user code before implementing main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_MAIN_UserInit(void)
{
    /* Start user code. Do not edit comment generated here */
	R_TAU0_Channel0_Start();
	R_TAU0_Channel6_Start();
    R_PCLBUZ0_Start();
    R_PCLBUZ1_Start();
    R_RTC_Start();
    R_IT_Start();
    R_UART0_Start();
    EI();
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/***********************************************************************************************************************
* Function Name: WaitForTicks
* Description  :
* Arguments    : none
* Return value : none
***********************************************************************************************************************/
void WaitForTicks(unsigned long WaitTicks)
{
    unsigned long T0_Tick;

    T0_Tick = GetSystemTicks();
    while ((GetSystemTicks() - T0_Tick) <  WaitTicks)
    {
        R_WDT_Restart();
    }
}
/*
 *
 */
void u0_EchoTest(void)
{
    unsigned char Data;
    unsigned short Timeout;
    MD_STATUS status;

    status = u0_GetcNonBlocking(&Data);
    if(status == MD_RX_DATA)
    {
        if(Data == 0x03u)
        {
            unsigned long T0_Tick, T1_Tick;
            unsigned char Ticks;
            static const __far unsigned char Display[] = {'1','2','3','4','5','6','7','8','9','.'};


            DBG_PUTS("Watchdog Timeout test\r\n") ;
            DBG_PUTS("Print a character every 62.5 milliseconds util the WDT reset occurs.\r\n") ;
#define TICK_RATE (64ul)
            T0_Tick = GetSystemTicks();
            while(1)
            {
                if ((GetSystemTicks() - T0_Tick) >= TICK_RATE)
                {
                    u0_Putc(Display[Ticks++]);
                    if(Ticks >= sizeof(Display)) Ticks = 0;

                    T0_Tick += TICK_RATE;
                }
            }
        }
        u0_PutcNonBlocking(Data);
    }
}
/*
 * Function: End_of_Test
 *
 * Note: The function never returns.
 */
void End_of_Test(void)
{
    unsigned long T0_Tick;

    DBG_PUTS("Begin echo test\r\n");
    T0_Tick = GetSystemTicks();
    while(1U)
    {
        if ((GetSystemTicks() - T0_Tick) >= 256ul)
        {
            T0_Tick += 256ul;
        }
        /* Do echo on UART0 */
        u0_EchoTest();

        /* Pet the dog */
        R_WDT_Restart();
    }
}

/***********************************************************************************************************************
* Function Name: Statics_Test
* Description  : Static variable test routine. The function replaces the
*        contents of the string ucStr with that of ucReplace, one
*        element at a time. Right-click the variable ucStr, and
*        select instant watch - click add in the subsequent dialog.
*        If you step through the function, you can watch the string
*        elements being overwritten with the new data.
* Arguments    : none
* Return value : none
***********************************************************************************************************************/
#define TICKS_BETWEEN_UPDATES (512ul)

void Statics_Test(void)
{
    /* Declare string variable to hold the string to be copied */
    uint8_t ucStr[] = "STATIC \0";
    /* Declare variable buffer to store the copied string */
    uint8_t ucReplace[] = "TESTTEST\0";

    /* Declare loop iteration count variable.
       Declared as volatile to prevent optimisation when compiled
       and executed using the Release build configuration  */
    volatile uint32_t ulDelay;
    volatile uint8_t uiCount;

    DBG_PUTS("Begin LCD module test\r\n") ;
    /* Write ucStr variable, "STATIC" to LCD.
       Casting used to ensure Display_LCD function parameters are met. */
    Display_LCD(LCD_LINE2, (const char *)ucStr);

    /* Delay */
    WaitForTicks(TICKS_BETWEEN_UPDATES);

    for (uiCount=0; uiCount<8; uiCount++)
    {
        ucStr[uiCount] = ' ';
    }
    Display_LCD(LCD_LINE2, (const char *)ucStr);

    /* Begin for loop which writes one letter of ucReplace to the LCD at a time
       The nested while loops generate the delay between each letter change */
    for (uiCount=0; uiCount<8; uiCount++)
    {
        /* Replace letter number uiCount of ucStr from ucReplace */
        ucStr[uiCount] = ucReplace[uiCount];

        /* Display the character on the debug LCD.
           Casting used to ensure Display_LCD function parameters are met. */
        Display_LCD(LCD_LINE2, (const char *)ucStr);

        /* LED Flashing Delay */
        WaitForTicks(TICKS_BETWEEN_UPDATES);
    }

    /* Clear LCD Display */
    ucStr[uiCount] = '\0';

    /* Write MCU nickname to LCD again */
    Display_LCD(LCD_LINE2, "TESTdone");
}
/***********************************************************************************************************************
End of function Statics_Test
***********************************************************************************************************************/

/* End user code. Do not edit comment generated here */
