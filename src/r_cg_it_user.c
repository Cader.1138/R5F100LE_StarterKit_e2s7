/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011, 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_it_user.c
* Version      : CodeGenerator for RL78/G13 V2.05.01.04 [18 May 2018]
* Device(s)    : R5F100LE
* Tool-Chain   : GCCRL78
* Description  : This file implements device driver for IT module.
* Creation Date: 8/11/2018
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_it.h"
/* Start user code for include. Do not edit comment generated here */
#include "r_cg_adc.h"
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
static unsigned long SystemTicks = 0;
/* Declare a variable for storing the ADC value */
volatile uint16_t gTimer_ADC_Period = 100;
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: r_it_interrupt
* Description  : This function is INTIT interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_it_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    static unsigned long LED_T0_Time = 0;

    SystemTicks++;
    if (SystemTicks - LED_T0_Time > gTimer_ADC_Period)
    {
        LED_T0_Time = SystemTicks;

        if(ADIF == 0)
        {
            /* start the next ADC cycle */
            R_ADC_Start();
        }
    }

    /* Clear Interval Timer interrupt flag */
    ITIF = 0U;    /* clear INTIT interrupt flag */

    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
unsigned long GetSystemTicks(void)
{
    unsigned long RetValue;

    RetValue = 0;
    if(ITMK == 0U)
    {
        ITMK = 1U;    /* disable INTIT interrupt */
        RetValue = SystemTicks;
        ITMK = 0U;    /* enable INTIT interrupt */
    }
    return RetValue;
}
/* End user code. Do not edit comment generated here */
