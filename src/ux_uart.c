/*
 * ux_uart.c
 *
 *  Created on: Apr 12, 2017
 *      Author: developer
 */
#include "r_cg_macrodriver.h"
#include "r_cg_serial.h"
#include "ux_uart.h"

typedef enum eu_Status{eTX_BufferBusyBit = 0x01, eRX_BufferBusyBit = 0x02, eRX_BufferAssignedBit = 0x04} eu_Status_t;

static unsigned char u0_TX_Buffer[1];
static unsigned char u0_RX_Buffer[1];
static volatile eu_Status_t   u0_Status = 0;

static unsigned char u2_TX_Buffer[1];
static unsigned char u2_RX_Buffer[1];
static volatile eu_Status_t   u2_Status = 0;

/*
 *
 */
void u0_TxCallBack(void)
{
    u0_Status &= ~eTX_BufferBusyBit;    /* mark buffer as sent */
}
/*
 *
 */
void u0_RxCallBack(void)
{
    u0_Status |=  eRX_BufferBusyBit;    /* mark buffer as full */
}
/*
 *
 */
MD_STATUS u0_Putc(const unsigned char c)
{
    MD_STATUS status;

    while(u0_Status & eTX_BufferBusyBit); /* needs a timeout otherwise the WDT will assert a reset when UART is busy */
    u0_TX_Buffer[0] = c;
    u0_Status |=  eTX_BufferBusyBit;
    status = R_UART0_Send(u0_TX_Buffer, sizeof(u0_TX_Buffer));
    return status;
}
/*
 *
 */
MD_STATUS u0_PutcNonBlocking(const unsigned char c)
{
    MD_STATUS status;

    if (u0_Status & eTX_BufferBusyBit)
    {
        status = MD_TX_BUSY;
    }
    else
    {
        u0_TX_Buffer[0] = c;
        u0_Status |=  eTX_BufferBusyBit;
        status = R_UART0_Send(u0_TX_Buffer, sizeof(u0_TX_Buffer));
    }
    return status;
}
/*
 *
 */
MD_STATUS u0_GetcNonBlocking(unsigned char * c)
{
    MD_STATUS status;

    if (u0_Status & eRX_BufferBusyBit)
    {
        if(c)
        {
            *c = u0_RX_Buffer[0];
        }
        u0_Status &= ~eRX_BufferBusyBit;
        u0_Status &= ~eRX_BufferAssignedBit;
        status = MD_RX_DATA;
    }
    else
    {
        if (!(u0_Status & eRX_BufferAssignedBit))
        {
            u0_Status |= eRX_BufferAssignedBit;
            status = R_UART0_Receive(u0_RX_Buffer, sizeof(u0_RX_Buffer));
        }
        else
        {
            status = MD_RX_WAITING;
        }
    }
    return status;
}
/*
 *
 */
MD_STATUS u0_Puts(const __far unsigned char * c)
{
    MD_STATUS status;
    unsigned char OutByte;

    if(c)
    {
        status = MD_OK;
        OutByte = *c;
        while(OutByte)
        {
            status = u0_Putc(OutByte);
            if(status == MD_OK)
            {
                R_WDT_Restart();
                c++;
                OutByte = *c;
            }
        }
    }
    else
    {
        status = MD_ARGERROR;
    }
    return status;
}
/*
 *
 */
void u2_TxCallBack(void)
{
    u2_Status &= ~eTX_BufferBusyBit;
}
/*
 *
 */
void u2_RxCallBack(void)
{
    u2_Status |=  eRX_BufferBusyBit;
}
/*
 *
 */
MD_STATUS u2_Putc(const unsigned char c)
{
    MD_STATUS status;

    while(u2_Status &  eTX_BufferBusyBit); /* needs a timeout otherwise the WDT will assert a reset when UART is busy */
    u2_TX_Buffer[0] = c;
    u2_Status |=  eTX_BufferBusyBit;
    status = R_UART2_Send(u2_TX_Buffer, sizeof(u2_TX_Buffer));
    return status;
}
/*
 *
 */
MD_STATUS u2_PutcNonBlocking(const unsigned char c)
{
    MD_STATUS status;

    if (u2_Status & eTX_BufferBusyBit)
    {
        status = MD_TX_BUSY;
    }
    else
    {
        u2_TX_Buffer[0] = c;
        u2_Status |=  eTX_BufferBusyBit;
        status = R_UART2_Send(u2_TX_Buffer, sizeof(u2_TX_Buffer));
    }
    return status;
}
/*
 *
 */
MD_STATUS u2_GetcNonBlocking(unsigned char * c)
{
    MD_STATUS status;

    if (u2_Status & eRX_BufferBusyBit)
    {
        if(c)
        {
            *c = u2_RX_Buffer[0];
        }
        u2_Status &= ~eRX_BufferBusyBit;
        u2_Status &= ~eRX_BufferAssignedBit;
        status = MD_RX_DATA;
    }
    else
    {
        if (!(u2_Status & eRX_BufferAssignedBit))
        {
            u2_Status |= eRX_BufferAssignedBit;
            status = R_UART2_Receive(u2_RX_Buffer, sizeof(u2_RX_Buffer));
        }
        else
        {
            status = MD_RX_WAITING;
        }
    }
    return status;
}
/*
 *
 */
MD_STATUS u2_Puts(const __far unsigned char * c)
{
    MD_STATUS status;
    unsigned char OutByte;

    if(c)
    {
        status = MD_OK;
        OutByte = *c;
        while(OutByte)
        {
            status = u2_Putc(OutByte);
            if(status == MD_OK)
            {
                R_WDT_Restart();
                c++;
                OutByte = *c;
            }
        }
    }
    else
    {
        status = MD_ARGERROR;
    }
    return status;
}
